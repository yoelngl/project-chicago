@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/css/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/gijgo.min.css') }}">
@endsection

@section('content')
div id="wrapper">

<div class="content-page">
    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Dashboard</h4>
                    </div>
                </div>
            </div>     
            <!-- end page title --> 
            <div class="row">
                                <div class="col-12">
                                    <div class="card-box table-responsive">
                                        <button type="button" name="create_record" id="create_record" class="btn btn-primary btn-sm sub-header text-white"><i class="fa fa-plus"></i></button>

                                        <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
    
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>1st Place</th>
                                                <th>2nd Place</th>
                                                <th>3rd Place</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
    
    
                                            <tbody>
                                                @foreach ($data as $lol)
                                            <tr>
                                                <td>{{ date('d-m-Y', strtotime($lol->tanggal)) }}</td>
                                                <td>{{ $lol->time }}</td>
                                                <td>{{ $lol->angka_1 }}</td>
                                                <td>{{ $lol->angka_2 }}</td>
                                                <td>{{ $lol->angka_3 }}</td>
                                                <td>
                                                    <a href="javascript:void(0)" class="edit" style="text-align: center;" id="{{ $lol->id }}"><i class="fa fa-edit text-info"></i></a>
                                                    &nbsp;
                                                    <a href="javascript:void(0)" class="delete" onclick="deleteConfirmation({{ $lol->id }})"><i class="fa fa-trash text-danger"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- end row -->
            <!--- end row -->
        </div> <!-- end container-fluid -->

    </div> <!-- end content -->
    {{-- Modal --}}
    <div id="formModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New</h4>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form method="post" id="sample_form" class="form-horizontal">
                    @csrf
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="control-label">Select Date:</label>
                                    <input type="text" name="tanggal" id="tanggal" class="form-control datepicker" placeholder="Select Date" autocomplete="off">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="control-label">Time:</label>
                                    <select name="waktu" id="waktu" class="form-control" autocomplete="off">
                                        <option value="">Select Time</option>
                                        <option value="09:30 AM">09:30 AM</option>
                                        <option value="11:30 AM">11:30 AM</option>
                                        <option value="06:30 PM">06:30 PM</option>
                                        <option value="08:00 PM">08:00 PM</option>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="control-label">1st Winner:</label>
                                    <input type="text" name="no_satu" id="no_satu" class="form-control" onkeypress="return isNumberKey(event)" maxlength="4" minlength="4" placeholder="0000" autocomplete="off">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="control-label">2nd Winner:</label>
                                    <input type="text" name="no_dua" id="no_dua" class="form-control" onkeypress="return isNumberKey(event)" maxlength="4" minlength="4" placeholder="0000" autocomplete="off">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="control-label">3rd Winner:</label>
                                    <input type="text" name="no_tiga" id="no_tiga" class="form-control" onkeypress="return isNumberKey(event)" maxlength="4" minlength="4" placeholder="0000" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group" align="center">
                                    <input type="hidden" name="action" id="action" value="Add">
                                    <input type="hidden" name="hidden_id" id="hidden_id">
                                    <input type="submit" name="action_button" value="Add" id="action_button" class="btn btn-primary btn-block">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Script --}}
    <script src="{{ asset('assets/js/vendor.min.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    {{-- <script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script> --}}
    <script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset("assets/libs/datatables/dataTables.responsive.min.js") }}"></script>
    <script src="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script> --}}
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
    <script src="{{ asset('assets/js/gijgo.min.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert2.all.min.js') }}"></script>


    

    <script>
        $('#datatable').dataTable({
            ordering:false,
        })
	    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

        $('#tanggal').datepicker({
        uiLibrary: 'bootstrap',
        format: 'yyyy-mm-dd',
    });

    $('#create_record').click(function(){
        $('.modal-title').text('Add New Record');
        $('#action_button').val('Add');
        $('#action').val('Add');
        $('#form_result').html('');
		$('#sample_form').trigger("reset");
        $('#formModal').modal('show');
    });

    $(document).on('click', '.edit', function(){
        var id = $(this).attr('id');
        $('#form_result').html('');
        $.ajax({
            url: '/admin/edit/'+id,
            dataType: 'json',
            success: function(data)
            {
                $('#tanggal').val(data.result.tanggal);
                $('#waktu').val(data.result.time);
                $('#no_satu').val(data.result.angka_1);
                $('#no_dua').val(data.result.angka_2);
                $('#no_tiga').val(data.result.angka_3);
                $('#hidden_id').val(data.result.id);
                $('.modal-title').text('Edit Record');
                $('#action_button').val('Edit');
                $('#action').val('Edit');
                $('#formModal').modal('show');
            }
        });
    });

    function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}

$('#sample_form').on('submit', function(event){
        event.preventDefault();
        var id = $(this).attr('id');
        var action_url  = '';

        if($('#action').val() == 'Add')
        {
            action_url = "{{ route('admin.add') }}";
        }

        if($('#action').val() == 'Edit')
        {
            action_url = "{{ route('admin.update') }}";
        }

        $.ajax({
            url: action_url,
            method: "POST",
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data)
            {
                var html = '';
                if(data.errors)
                {
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < data.errors.length; count++)
                    {
                        html += '<li><b>'+data.errors[count]+'</b></li>';
                    }
                    html += '</div>';
                }

                if (data.error) {
                    Swal.fire({
                        icon: 'error',
                        title: "Time already exist!",
                        text: "Change your time option",
                    });
                }

                if(data.success)
                {
                    Swal.fire({
                        // position: "top",
                        // toast: true,
                        icon: 'success',
                        title: "Success",
                        text: "Data save successfully!",
                        // showConfirmButton: false,
                        // timer: 3000
                    }).then(function() {
                        location.reload();
                    });
                    $('#formModal').modal('hide');
                    // html = '<div class="alert alert-success">'+data.success+'</div>';
                    $('#sample_form')[0].reset();
                }
                $('#form_result').html(html);
            }
        });
    });

    function deleteConfirmation(id) {
        console.log(id)
    Swal.fire({
        icon: 'warning',
        title: "Delete?",
        text: "Please ensure and then confirm!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, Cancel it!",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: '/admin/delete/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Data Deleted",
                            // showConfirmButton: false,
                            // timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

$(document).ready(function () {
            $('#pass').on('click', function() {
                $('#modals').modal('show');
            });

            $('#form').on('submit', function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('admin.change') }}",
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            Swal.fire({
                                icon: 'success',
                                type: 'success',
                                text: 'Password changed!',
                            }).then(function () {
                                location.reload();
                            });
                            $('#form').modal('hide');
                        }
                    }
                });
            });
        });
    </script>
@endsection