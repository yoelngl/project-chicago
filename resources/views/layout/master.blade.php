<!DOCTYPE html>
<html lang="en">

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{ asset('assets/wp-content/themes/lotteryv1/images/logo-chicago.png') }}">
  <meta name="viewport" content="width=1366">
<title> 
Chicago Lottery | Results &amp; Winning Lottery Numbers</title>
	<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel='stylesheet' id='wp-block-library-css'  href="{{ asset('assets/wp-includes/css/dist/block-library/style.min6e7a.css?ver=5.2.7') }}" type='text/css' media='all' />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >

<link rel='stylesheet' id='httpunderscores-me-style3-css'  href="{{ asset('assets/wp-content/themes/lotteryv1/css/flipclock6e7a.css?ver=5.2.7') }}" type='text/css' media='all' />
<link rel='stylesheet' id='httpunderscores-me-style-css'  href="{{ asset('assets/wp-content/themes/lotteryv1/css/mainc64e.css?ver=1.1.1') }}" type='text/css' media='all' />
<link rel="manifest" href="https://wilottery.com/manifest.json">
<link rel="stylesheet" href="{{ asset ('assets/Past-Winning-Numbers/all.css') }}" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.uikit.min.css">
<link rel="stylesheet" href="{{ asset ('assets/css/uikit.min.css') }}">
<link href="{{ asset ('assets/Past-Winning-Numbers/hidden.module.css') }}" rel="stylesheet">


<link rel="stylesheet" media="all" href="{{ asset ('assets/Past-Winning-Numbers/styles.min.css') }}">
<!-- <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.uikit.min.js
"></script> -->
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/jquery6e7a.js?ver=5.2.7') }}"></script>
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/template74ef.js?ver=995.75.90') }}"></script>

		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
			<script>
	window.onload=function(){
    check();
		countdown();
  };
  
	</script>
  
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body id="buriq2">
  <div class="wrapper">
    <div class="navbar navbar-inverse text-center">
      <div class="container">
          <ul class="nav navbar-nav">
            <li class="logo"><a href="/"><img src="{{ asset('assets/wp-content/themes/lotteryv1/images/logo-chicago.png') }}" alt="Chicago Lottery" class="img-logo"></a></li>
          </ul>
      </div>
    </div>

    @yield('content')
    <img style="position:relative;bottom:130px;left:90px;" src="{{ asset ('assets/wp-content/themes/lotteryv1/images/bnv.png') }}" alt="" height="45">
    <img style="position:relative;bottom:130px;left:570px;" src="{{ asset ('assets/wp-content/themes/lotteryv1/images/vip.png') }}" alt="">
  <footer id="footer">
  <div class="footer1" style="background-color:#CCCCCC;">
  
  <table width="950" border="0" align="center" cellpadding="0" cellspacing="0" style="top:30px;position:relative;">
			<tbody><tr>
				<td align="left" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody><tr>
							<td width="250" align="justify" valign="bottom" style="background-color:#CCCCCC; height: 40px; margin-top: -49px;margin-left: 130px;" class="foot"><img src="assets/wp-content/themes/lotteryv1/images/chicago.png" alt="Chicago Lottery" width="220"></td>
							<td align="justify" valign="middle" style="color:black;">
							<p class="px-md-1 mx-md-5">	Draws take place every day and every Morning, Evening and Night. players are required to choose four main numbers between 0 and 9 plus an additional number, known as the Superzahl, between and 9. To win the jackpot, a player must match all six numbers, but prizes are available for matching as few as four main numbers plus the Superzahl.</p>
							</td>
						</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody></table>
    
          <div class="container text-xs-center small py-2">

    </div></b></b>
            
  </div>
  <div class="footer2">
    <div class="container">
      <div class="row">
        <div class="col"></div>
        <div class="col">
          <div class="widget-body">
              <p class="text-middle">
              Chicago Lottery.com Copyright &copy; 2001, All Rights Reserved
              </p>
            </div>
        </div>
        <div class="col"></div>
      </div>
    </div>
  </div>

  </footer> 
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/flipclock.min4963.js?ver=1.1') }}"></script>
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/carousel4963.js?ver=1.1') }}"></script>
<script type='text/javascript' src="{{ asset('assets/wp-includes/js/wp-embed.min6e7a.js?ver=5.2.7') }}"></script>
</body>

</html>
