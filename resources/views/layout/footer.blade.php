<footer id="footer">
  <div class="footer1">
    <div class="container">
      <div class="row text-center">
        <p>Every attempt is made to ensure the information displayed on this website is accurate. However, in the event of any discrepancies, the official records maintained by Chicago Lottery shall prevail.</p>
        <p>Chicago Lottery does not intend for any of the information contained on this website to be used for illegal purposes. The information in this site is for news and entertainment purposes only.</p>
        <p>Must be at least 18 years or older to play.</p>
      </div>
      <div class="col-xs-12 text-center">
          <div class="row leg">
            <img src="{{ asset('assets/wp-content/themes/lotteryv1/images/play-responsibly.png') }}" />
            <img src="{{ asset('assets/wp-content/themes/lotteryv1/images/apla.png') }}" />
            <img src="{{ asset('assets/wp-content/themes/lotteryv1/images/wla.png') }}" />
            <img src="{{ asset('assets/wp-content/themes/lotteryv1/images/18-plus.png') }}" />
          </div>
      </div>
    </div>
  </div>
  <div class="footer2">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 widget">
          <div class="widget-body">
            <p>
              <a href="{{ url('home') }}">Home</a> |
              <a href="{{ url('result') }}">Results</a> |
              <a href="{{ url('responsible') }}">Responsible Gaming</a> |
              <a href="{{ url('contact') }}">Contact Us</a>
            </p>
          </div>
        </div>
        <div class="col-xs-6 widget">
          <div class="widget-body">
            <p class="text-right">
              Copyright &copy; {{ date('Y') }}, Chicago Lottery. All Rights Reserved
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer> </div>
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/bootstrap.min4963.js?ver=1.1') }}"></script>
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/flipclock.min4963.js?ver=1.1') }}"></script>
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/carousel4963.js?ver=1.1') }}"></script>
<script type='text/javascript' src="{{ asset('assets/wp-includes/js/wp-embed.min6e7a.js?ver=5.2.7') }}"></script>
</body>

<!-- Mirrored from chicagolottery.us/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 13 Jun 2020 15:27:46 GMT -->
</html>