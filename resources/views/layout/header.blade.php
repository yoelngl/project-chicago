<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from chicagolottery.us/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 13 Jun 2020 15:27:28 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{ asset('assets/wp-content/themes/lotteryv1/images/favicon.png') }}">
  <meta name="viewport" content="width=1366">
<title> 
Chicago Lottery | Results &amp; Winning Lottery Numbers</title>
	<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel='stylesheet' id='wp-block-library-css'  href="{{ asset('assets/wp-includes/css/dist/block-library/style.min6e7a.css?ver=5.2.7') }}" type='text/css' media='all' />
<link rel='stylesheet' id='httpunderscores-me-style2-css'  href="{{ asset('assets/wp-content/themes/lotteryv1/css/bootstrap.min6e7a.css?ver=5.2.7') }}" type='text/css' media='all' />
<link rel='stylesheet' id='httpunderscores-me-style3-css'  href="{{ asset('assets/wp-content/themes/lotteryv1/css/flipclock6e7a.css?ver=5.2.7') }}" type='text/css' media='all' />
<link rel='stylesheet' id='httpunderscores-me-style-css'  href="{{ asset('assets/wp-content/themes/lotteryv1/css/mainc64e.css?ver=1.1.1') }}" type='text/css' media='all' />
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/jquery6e7a.js?ver=5.2.7') }}"></script>
<script type='text/javascript' src="{{ asset('assets/wp-content/themes/lotteryv1/js/template74ef.js?ver=995.75.90') }}"></script>

		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
			<script>
	window.onload=function(){
    check();
		countdown();
	};
	</script>
  
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body id="buriq2">
  <div class="wrapper">
    <div class="navbar navbar-inverse text-center" >
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="/"><img src="{{ asset('assets/wp-content/themes/lotteryv1/images/chicago.png') }}" alt="Chicago Lottery" class="img-logo"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="{{ url('result') }}">Results</a></li>
            <li class="logo"><a href="/"><img src="{{ asset('assets/wp-content/themes/lotteryv1/images/chicago.png') }}" alt="Chicago Lottery" class="img-logo"></a></li>
            <li><a href="{{ url('responsible') }}">Responsible Gaming</a></li>
            <li><a href="{{ url('contact') }}">Contact Us</a></li>
          </ul>
        </div>
      </div>
    </div>	<hr>		