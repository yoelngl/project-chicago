@extends('layout.master')

@section('content')
    <div class="container">
			<h2 class="text-center white"><?= $days ?>, <?= $tanggal; ?></h2>
			<div class="banner">LIVE DRAW Lotto Result : <?= $days; ?>
			<!--//<div class="start">17:00:00</div> - <div class="until">17:30:00</div> //--></div>
			<div class="col-xs-8 col-xs-push-2" style="padding:0">
            <div class="panel panel-warning" style="border-radius:0">
      

                    <div class="panel-body" style="padding:0">
                      @foreach($data as $row)
                      <table class="table table-bordered text-center" style="margin:0">
                        
                        <thead><tr class="bg-primary"><th>1st Prize</th><th>2nd Prize</th><th>3rd Prize</th></tr></thead>
                        <tbody><tr class="prize">
                          <td>
                          @for($i=0 ; $i <= 3; $i++)
                          <img src="{{ asset('assets/images/chicago/'.$row->angka_1[$i].'.png') }}" width="20px" height="50px" class="mr-2" alt="" /> 
                        @endfor
                        </td>
                        <td>
                          @for($i=0 ; $i <= 3; $i++)
                          <img src="{{ asset('assets/images/chicago/'.$row->angka_2[$i].'.png') }}" width="20px" height="50px" class="mr-2" alt="" /> 
                        @endfor
                        </td>
                        <td>
                          @for($i=0 ; $i <= 3; $i++)
                          <img src="{{ asset('assets/images/chicago/'.$row->angka_3[$i].'.png') }}" width="20px" height="50px" class="mr-2" alt="" /> 
                        @endfor
                        </td>

                        </tbody>

                         </tbody> </table>
                      @endforeach
                    </div>
                  </div>
			</div>
			
			<div class="clearfix"></div>										
			<div class="clearfix"></div>
		</div><br><br><br>
    <h1 style="text-align: center; color: white;"><strong>Past Winning Number</strong></h1>
		
	            <div class="container content">
              <table id="example" class="uk-table uk-table-hover uk-table-striped " style="width:100%;color:white;">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Morning</th>
                <th>Midday</th>
                <th>Evening</th>
                <th>Night</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $tadi }}</td>
                <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>
            <tr>
                <td><b>{{ $ytd }}</b></td>
                <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>

            <tr>
                <td><b>{{ $ytd2 }}</b></td>
             <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>

            <tr>
                <td><b>{{ $ytd3 }}</b></td>
             <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>

          <tr>
                <td><b>{{ $ytd4 }}</b></td>
             <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>

           <tr>
                <td><b>{{ $ytd5 }}</b></td>
            <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>

           <tr>
                <td><b>{{ $ytd6 }}</b></td>
              <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>
            
            <tr>
                <td><b>{{ $ytd7 }}</b></td>
                <td>@if($pagiTadi == null)
                1321
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                3213
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                3214
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                1423
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="20px" height="30px">
                @endfor
                @endif</td>
            </tr>
        </tbody>
    </table>
    
    <!-- Jumbotron -->
<div class="card card-image" style="background-color: black;opacity:0.7;margin-bottom:50px;font-weight:900;">
  <div class="text-white text-center rgba-stylish-strong py-5 px-4">
    <div>

      <!-- Content -->
      
      <h2 class="card-title h2 my-4" style="color: gold;">Did You Win?</h2>
      <p class="px-md-5 mx-md-5">What would you do with a life-changing jackpot? Dream BIG and explore the possibilities when you purchase your chance to win among 3 prize and easy-to-play draw games. Whether you play your own lucky numbers or throw caution to the wind with quick pick, it is fun to dream a bit, and absolutely exhilarating to win a big prize.</p>
      <a href="{{ url('result')}}"><button type="button" class="btn btn-secondary btn-rounded" style="backgound-color:red;">More result</button></a>

    </div>
    
    
  </div>
</div>
<img src="http://versailleslottery.com/images/bnv.png" alt="" class="float-left" height="45">
                <img src="http://versailleslottery.com/images/vip.png" alt="" class="float-right">
<!-- Jumbotron -->
                </div>
                
	<div class="clearfix"></div>
  
@endsection