@extends('layout.master')

@section('content')
    
    <div class="row" style="margin-right: 0px;margin-bottom:50px;">
      <div class="col"></div>
        <div class="col-10">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" >
                  <div class="carousel-item active"><a href="chicagosuperlottery.com">
                    <img src="{{ asset ('assets/images//iklan/iklan2.png') }}" class="d-block w-100" alt="..." height="261"></a>
                  </div>
                  <div class="carousel-item"><a href="chicagosuperlottery.com">
                    <img src="{{ asset ('assets/images//iklan/iklan3.png') }}" class="d-block w-100" alt="..." height="260">
                  </a></div>
                  <div class="carousel-item"><a href="chicagosuperlottery.com">
                    <img src="{{ asset ('assets/images//iklan/iklan1.png') }}" class="d-block w-100" alt="..." height="260">
                  </a></div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
          </div>
      <div class="col"></div>
    </div>
        
    
    <div class="container">
			<h2 class="text-center white"><?= $hari ?>, <?= $tanggal; ?></h2>
			<div class="banner">LIVE DRAW Lotto Result : <?= $hari; ?>
			<!--//<div class="start">17:00:00</div> - <div class="until">17:30:00</div> //--></div>
			<div class="col-xs-8 col-xs-push-2" style="padding:0">
            <div class="panel panel-warning" style="border-radius:0">
      

                    <div class="panel-body" style="padding:0">
                      <table class="table table-bordered text-center" style="margin:0">
                        
                        <thead><tr class="bg-primary">
                            <th>1st Prize</th>
                            <th>2nd Prize</th>
                            <th>3rd Prize</th>
                        </tr>
                        </thead>
                        <tbody><tr class="prize">
                            <?php foreach ($data as $row): ?>
                                
                          <td>
                          @for($i=0 ; $i <= 3; $i++)
                          <img src="{{ asset('assets/images/chicago/'.$row->angka_1[$i].'.png') }}" width="20px" height="50px" class="mr-2" alt="" />  
                        @endfor
                        </td>

                        <td>
                          @for($i=0 ; $i <= 3; $i++)
                          <img src="{{ asset('assets/images/chicago/'.$row->angka_2[$i].'.png') }}" width="20px" height="50px" class="mr-2" alt="" /> 
                        @endfor
                        </td>
                        <td>
                          @for($i=0 ; $i <= 3; $i++)
                          <img src="{{ asset('assets/images/chicago/'.$row->angka_3[$i].'.png') }}" width="20px" height="50px" class="mr-2" alt="" /> 
                        @endfor
                        </td>
                            <?php endforeach ?>

                        
                        </tbody>
                         </table>
                    </div>
                  </div>
            <div class="row" style="margin-top:50px;">
              <div class="col">
              </div>      
                    <div class="col-9">
                          <table width="100%"  cellspacing="0" cellpadding="0">
                              <tbody>
                              <tr>
                                    <td><a href="#"><img src="{{ asset ('assets/images/partner/4.gif') }}" width="90%" style="height:150px;"></a></td>
                                    <td><a href="#"><img src="{{ asset ('assets/images/partner/3.gif') }}" width="90%" style="height:150px;  "></a></td>
                                    <td><a href="#"><img src="{{ asset ('assets/images/partner/2.gif') }}" width="90%" style="height:150px; "></a></td>
                                    <td><a href="#"><img src="{{ asset ('assets/images/partner/1.gif') }}" width="90%" style="height:150px; "></a></td>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                      </div>
                  <div class="col">
                  </div>
            </div>
			</div>		
			<div class="clearfix"></div>										
			<div class="clearfix"></div>
    </div><br><br><br>
          
    <h1 style="text-align: center; color: white;"><strong>Past Winning Number</strong></h1>
		
	            <div class="container content">
              <table id="example" class="uk-table uk-table-striped" style="width:100%;color:black!important;background:rgba(255,255,255,.6);">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Morning</th>
                <th>Midday</th>
                <th>Evening</th>
                <th>Night</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $tadi }}</td>
                <td>@if($pagiTadi == null)
                -
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pagiTadi[$i].'.png')}}" alt="" width="25px" height="40px">
                @endfor
                @endif</td>
                <td>@if($siangTadi == null)
                -
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $siangTadi[$i].'.png')}}" alt="" width="25px" height="40px">
                @endfor
                @endif</td>
                <td>@if($soreTadi == null)
                -
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $soreTadi[$i].'.png')}}" alt="" width="25px" height="40px">
                @endfor
                @endif</td>
                <td>@if($malamTadi == null)
                -
                  @else
                  @for($i=0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $malamTadi[$i].'.png')}}" alt="" width="25px" height="40px">
                @endfor
                @endif</td>
            </tr>
            <tr style="background-color: rgba(210, 215, 211, 1);">
                <td><b>{{ $ytd }}</b></td>
              @foreach($pagi as $pg) 
              <?php $var = ($pg->tanggal->format('M j') == $ytd);
              if($var == null) : ?>

              <!-- <td> - </td> -->
              @elseif($pg->tanggal->format('M j') == $ytd)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($siang as $sg)

                <?php $var = ($sg->tanggal->format('M j') == $ytd);
              if($var == "") : ?>
                
              @elseif($sg->tanggal->format('M j') == $ytd)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

               @foreach($sore as $sr)

               <?php $var = ($sr->tanggal->format('M j') == $ytd);
              if($var == null) : ?>

              <!-- <td> - </td> -->

              @elseif($sr->tanggal->format('M j') == $ytd)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sr->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($malam as $ml)
                <?php $var = ($ml->tanggal->format('M j') == $ytd);
              if($var == null) : ?>

              <!-- <td> - </td> -->
              @elseif($ml->tanggal->format('M j') == $ytd)
                <td>
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $ml->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach
            </tr>
            <tr>
                <td><b>{{ $ytd2 }}</b></td>
              @foreach($pagi as $pg)
              <?php $var = ($pg->tanggal->format('M j') == $ytd2);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($pg->tanggal->format('M j') == $ytd2)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($siang as $sg)
                <?php $var = ($sg->tanggal->format('M j') == $ytd2);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sg->tanggal->format('M j') == $ytd2)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

               @foreach($sore as $sr)
               <?php $var = ($sr->tanggal->format('M j') == $ytd2);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sr->tanggal->format('M j') == $ytd2)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sr->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($malam as $ml)
                <?php $var = ($ml->tanggal->format('M j') == $ytd2);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($ml->tanggal->format('M j') == $ytd2)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $ml->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach
            </tr>
            <tr style="background-color: rgba(210, 215, 211, 1);">
                <td><b>{{ $ytd3 }}</b></td>
              @foreach($pagi as $pg)
              <?php $var = ($pg->tanggal->format('M j') == $ytd3);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($pg->tanggal->format('M j') == $ytd3)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($siang as $sg)
                <?php $var = ($sg->tanggal->format('M j') == $ytd3);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sg->tanggal->format('M j') == $ytd3)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

               @foreach($sore as $sr)
               <?php $var = ($sr->tanggal->format('M j') == $ytd3);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sr->tanggal->format('M j') == $ytd3)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sr->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($malam as $ml)
                <?php $var = ($ml->tanggal->format('M j') == $ytd3);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($ml->tanggal->format('M j') == $ytd3)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $ml->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach
            </tr>
          <tr>
                <td><b>{{ $ytd4 }}</b></td>
              @foreach($pagi as $pg)
              <?php $var = ($pg->tanggal->format('M j') == $ytd4);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($pg->tanggal->format('M j') == $ytd4)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($siang as $sg)
                <?php $var = ($sg->tanggal->format('M j') == $ytd4);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sg->tanggal->format('M j') == $ytd4)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

               @foreach($sore as $sr)
               <?php $var = ($sr->tanggal->format('M j') == $ytd4);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sr->tanggal->format('M j') == $ytd4)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sr->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($malam as $ml)
                <?php $var = ($ml->tanggal->format('M j') == $ytd4);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($ml->tanggal->format('M j') == $ytd4)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $ml->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach
            </tr>
           <tr style="background-color: rgba(210, 215, 211, 1);">
                <td><b>{{ $ytd5 }}</b></td>
              @foreach($pagi as $pg)
              <?php $var = ($pg->tanggal->format('M j') == $ytd5);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($pg->tanggal->format('M j') == $ytd5)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($siang as $sg)
                <?php $var = ($sg->tanggal->format('M j') == $ytd5);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sg->tanggal->format('M j') == $ytd5)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

               @foreach($sore as $sr)
               <?php $var = ($sr->tanggal->format('M j') == $ytd5);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sr->tanggal->format('M j') == $ytd5)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sr->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($malam as $ml)
                <?php $var = ($ml->tanggal->format('M j') == $ytd5);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($ml->tanggal->format('M j') == $ytd5)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $ml->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach
            </tr>
           <tr>
                <td><b>{{ $ytd6 }}</b></td>
              @foreach($pagi as $pg)
              <?php $var = ($pg->tanggal->format('M j') == $ytd6);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($pg->tanggal->format('M j') == $ytd6)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($siang as $sg)
                <?php $var = ($sg->tanggal->format('M j') == $ytd6);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sg->tanggal->format('M j') == $ytd6)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

               @foreach($sore as $sr)
               <?php $var = ($sr->tanggal->format('M j') == $ytd6);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sr->tanggal->format('M j') == $ytd6)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sr->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($malam as $ml)
                <?php $var = ($ml->tanggal->format('M j') == $ytd6);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($ml->tanggal->format('M j') == $ytd6)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $ml->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach
            </tr>
            <tr style="background-color: rgba(210, 215, 211, 1);">
                <td><b>{{ $ytd7 }}</b></td>
              @foreach($pagi as $pg)
              <?php $var = ($pg->tanggal->format('M j') == $ytd7);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($pg->tanggal->format('M j') == $ytd7)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $pg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($siang as $sg)
                <?php $var = ($sg->tanggal->format('M j') == $ytd7);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sg->tanggal->format('M j') == $ytd7)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sg->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

               @foreach($sore as $sr)
               <?php $var = ($sr->tanggal->format('M j') == $ytd7);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($sr->tanggal->format('M j') == $ytd7)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $sr->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach

                @foreach($malam as $ml)
                <?php $var = ($ml->tanggal->format('M j') == $ytd7);
              if($var == null) : ?>

            <!--   <td> - </td> -->
              @elseif($ml->tanggal->format('M j') == $ytd7)
                <td >
                    @for($i = 0; $i <= 3; $i++)
                    <img class="mobile" src="{{asset ('assets/images/chicago/'. $ml->angka_1[$i] .'.png')}}" alt="" width="25px" height="40px">
                    @endfor
                   
                </td>
                
                @endif
                @endforeach
            </tr>
        </tbody>
    </table>
    <!-- Jumbotron -->
<div class="card card-image" style="background-color: black;opacity:0.7;margin-bottom:50px;font-weight:900;">
  <div class="text-white text-center rgba-stylish-strong py-5 px-4">
    <div>

      <!-- Content -->
      
      <h2 class="card-title h2 my-4" style="color: gold;">Did You Win?</h2>
      <p class="px-md-5 mx-md-5">What would you do with a life-changing jackpot? Dream BIG and explore the possibilities when you purchase your chance to win among 3 prize and easy-to-play draw games. Whether you play your own lucky numbers or throw caution to the wind with quick pick, it is fun to dream a bit, and absolutely exhilarating to win a big prize.</p>
      <a href="{{ url('result')}}"><button type="button" class="btn btn-secondary btn-rounded">More result</button></a>

    </div>
  </div>
</div>
<!-- Jumbotron -->
                </div>
	<div class="clearfix"></div>
  
@endsection