@extends('layout.master')

@section('content')		
		
	<body class="color-two wl-winners" auth="0">
		
		  <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas="">
    
	

	

	

	

	

	<main role="main" class="main-content">
		
		  <div>
    <div data-drupal-messages-fallback="" class="hidden"></div>
    <div id="block-wilottery-content">
  
    
      

<section class="thin-hero">
    
    <div class="container">
        <div class="row">
            <div class="col col-md-12 offset-lg-1 col-lg-5">
                <h1><span>Past Winning Numbers</span>
</h1>

                				<div class="subtext">
				
				</div>
				
                            </div>
        </div>
    </div>
</section>
<section class="all-winners-numbers draw-history">
	<div class="container">
		<div class="row">
			<div class="col col-12">
                
                 <div class="draw-results-titles d-none d-lg-block">
                    <div class="row no-margin">
                        <div class="col-12 title-content">
                            <div class="row colset">
                                <div class="col col-lg-2 date">Date</div>
                                <div class="col col-lg-2">Game</div>
                                <div class="col col-lg-5 numbers">Winning Numbers</div>
                                <div class="col col-lg-3">Estimated Jackpot</div>
                            </div>
                        </div>
                    </div>
                </div>

                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">SuperCash!</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">3</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">4</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">14</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">29</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">34</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number dblr on" title="">
                                                    <span class="prefix ">N</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday, Evening</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 3</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">4</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday, Midday</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 3</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">0</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday, Evening</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 4</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">2</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">0</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday, Midday</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 4</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">4</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">7</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Badger 5</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">15</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">20</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">23</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">24</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">25</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"><span class="d-lg-none">Estimated Jackpot - </span>$10,000.00</div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday, Evening</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">All or Nothing</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">2</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">3</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w9 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w7 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">11</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">15</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w8 on" title="">
                                                    <span class="prefix ">16</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w11 on" title="">
                                                    <span class="prefix ">18</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">20</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">21</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w10 on" title="">
                                                    <span class="prefix ">22</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Monday, Midday</div>
									<strong>06/15/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">All or Nothing</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">2</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">4</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w7 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w8 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w9 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">10</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">11</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">12</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w10 on" title="">
                                                    <span class="prefix ">16</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w11 on" title="">
                                                    <span class="prefix ">17</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">20</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">SuperCash!</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">16</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">20</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">23</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">27</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">30</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number dblr on" title="">
                                                    <span class="prefix ">Y</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday, Evening</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 3</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">0</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">2</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">4</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday, Midday</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 3</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">3</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday, Evening</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 4</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">4</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday, Midday</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 4</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">0</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">7</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">3</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">7</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Badger 5</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">8</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">21</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">22</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">29</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"><span class="d-lg-none">Estimated Jackpot - </span>$80,000.00</div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday, Evening</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">All or Nothing</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w8 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">3</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w7 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w10 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">12</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w11 on" title="">
                                                    <span class="prefix ">13</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">14</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">15</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w9 on" title="">
                                                    <span class="prefix ">17</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">18</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">19</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Sunday, Midday</div>
									<strong>06/14/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">All or Nothing</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w7 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">8</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w9 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">10</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w11 on" title="">
                                                    <span class="prefix ">11</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w10 on" title="">
                                                    <span class="prefix ">12</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w8 on" title="">
                                                    <span class="prefix ">13</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">15</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">16</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">18</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">19</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Saturday</div>
									<strong>06/13/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">SuperCash!</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">2</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">10</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">22</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">24</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">39</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number dblr on" title="">
                                                    <span class="prefix ">N</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Saturday</div>
									<strong>06/13/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 3</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">2</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">8</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Saturday</div>
									<strong>06/13/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 4</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">8</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Saturday</div>
									<strong>06/13/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Badger 5</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">15</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">16</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">29</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">30</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"><span class="d-lg-none">Estimated Jackpot - </span>$61,000.00</div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Saturday</div>
									<strong>06/13/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Powerball</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">2</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">12</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">32</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">50</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">65</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number pb on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number pplayno on" title="">
                                                    <span class="prefix ">3x</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"><span class="d-lg-none">Estimated Jackpot - </span>$20.00M</div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Saturday</div>
									<strong>06/13/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Megabucks</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">8</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">14</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">19</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">42</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">43</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"><span class="d-lg-none">Estimated Jackpot - </span>$1.00M</div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Saturday</div>
									<strong>06/13/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">All or Nothing</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w10 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w11 on" title="">
                                                    <span class="prefix ">6</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">8</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w7 on" title="">
                                                    <span class="prefix ">11</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">15</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">17</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">18</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">20</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w9 on" title="">
                                                    <span class="prefix ">21</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w8 on" title="">
                                                    <span class="prefix ">22</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Friday</div>
									<strong>06/12/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">SuperCash!</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w4 on" title="">
                                                    <span class="prefix ">4</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">9</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">22</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w5 on" title="">
                                                    <span class="prefix ">24</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w6 on" title="">
                                                    <span class="prefix ">29</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">37</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number dblr on" title="">
                                                    <span class="prefix ">N</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="winning-numbers-line">
                    <div class="row no-margin">
                        <div class="col-12 numbers-content">
                            <div class="row colset">
                                <div class="col-12 col-lg-2 date">
                                	<div>Friday</div>
									<strong>06/12/2020</strong>
								</div>
                                <div class="col-12 col-lg-2 game">Pick 3</div>
                                <div class="col-12 col-lg-5 numbers-drawn">
                                    <div class="winning-numbers small-ball">
                                        <ul>
                                                                                        <li>
                                                <div class="winning-number w1 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w2 on" title="">
                                                    <span class="prefix ">5</span>
                                                </div>
                                            </li>
                                                                                        <li>
                                                <div class="winning-number w3 on" title="">
                                                    <span class="prefix ">1</span>
                                                </div>
                                            </li>
                                                                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-3 estimated-jackpot"></div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>
  </div>

  </div>


		
					<aside class="layout-sidebar-second" role="complementary">
				
			</aside>
		
	</main>

	  <div>
    




  </div>

@endsection