@extends('layout.master')

@section('content')
    <div class="container content">
      <div class="column">
        <ol class="breadcrumb" style="background-color: #a2e0f2">
          <li><a href="../index.html">Home</a></li>
          <li class="active">Results</li>
        </ol>
        <div class="row">
          <article class="col-sm-12 maincontent">
            <header class="page-header">
              <h1 class="page-title"> Results</h1>
            </header>
                <!-- <div class="form-group form-inline pull-right">
                  <a href="index.html" class="btn btn-primary">All Day</a> <a href="index9536.html?day=Monday" class="btn btn-primary">Monday</a> <a href="index52f1.html?day=Tuesday" class="btn btn-primary">Tuesday</a> <a href="index90aa.html?day=Wednesday" class="btn btn-primary">Wednesday</a> <a href="indexc4fc.html?day=Thursday" class="btn btn-primary">Thursday</a> <a href="index7d9b.html?day=Friday" class="btn btn-primary">Friday</a> <a href="index09bd.html?day=Saturday" class="btn btn-primary">Saturday</a> <a href="index6692.html?day=Sunday" class="btn btn-primary">Sunday</a>
                </div> -->
                <div class="clearfix"></div>   
                <table id="example" class="uk-table uk-table-striped" style="width:100%;color:black;">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Waktu</th>
                <th>1st Place</th>
                <th>2nd Place</th>
                <th>3rd Place</th>
            </tr>
        </thead>
        <tbody>
           <?php foreach ($results as $item): ?>
                @if($item->tanggal->format('Y-m-d') != $skr && $item->tanggal->format('Y-m-d') != $tom)
                <tr>
                    <td>{{ $item->tanggal->format('d-m-Y') }}</td>
                    <td>{{ $item->time }}</td>
                    <td>{{ $item->angka_1 }}</td>
                    <td>{{ $item->angka_2 }}</td>
                    <td>{{ $item->angka_3 }}</td>




                </tr>
                @endif
            <?php endforeach ?>
        </tbody>
    </table>
        </article>
        
        </div>
      </div>
   </div>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

   <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable ({
        ordering:false
    });
} );
   </script>
@endsection
