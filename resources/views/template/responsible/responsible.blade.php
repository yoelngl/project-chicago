@extends('layout.master')

@section('content')
    <hr>   <div class="container content">
      <div class="column">
        <ol class="breadcrumb" style="background-color: #a2e0f2">
          <li><a href="../index.html">Home</a></li>
          <li class="active">Responsible Gaming</li>
        </ol>
        <div class="row">
          <article class="col-sm-8 maincontent">
            <header class="page-header">
              <h1 class="page-title">Responsible Gaming</h1>
            </header>
            <h3>PLAY RESPONSIBLE</h3>
            <p>It is Chicago Lottery wish that our players have some fun playing our games. It should be treated as part of entertainment and should not adversely affect your finances or lifestyles. Our game rules state that no person under the age of 21 years shall be allowed to purchase a ticket or claim any prizes. Please exit this website immediately if you are under 21. Chicago Lottery is committed to be a responsible organization and encourage players to Play Responsibly.</p>
            <h3>Tips to keep Gambling fun</h3>
            <p>
              <ul>
                <li>Play for entertainment. As with all forms of entertainment, there is a cost involved. The more you play, the more you pay.</li>
                <li>Play within your means. Set a budget and stick to it. Stop playing if you exceed that limit.</li>
                <li>Use money you can afford to spend. Never spend more to win back losses.</li>
                <li>Keep balance in your life. Do not be pressurized or influenced by others to bet. Betting should be your own choices.</li>
              </ul>
            </p>
            <h3>Signs of Problem Gambling</h3>
            <p>
              <ul>
                <li>Spending more time or money on gambling than is affordable or planned.</li>
                <li>Borrowing money to gamble.</li>
                <li>Gambling with money meant for essentials like food or rent.</li>
                <li>Neglecting important responsibilities like work, school or family to gamble.</li>
                <li>Lying about the extent of gambling.</li>
                <li>Chasing losses to try and get money back.</li>
                <li>Increasing arguments with friends and family, especially about money issues.</li>
                <li>Increasing debts due to gambling.</li>
                <li>Feeling a sense of emptiness or loss when not gambling.</li>
                <li>Finding it difficult to control, stop, or cut down gambling or feeling irritable when trying to do so.</li>
                <li>Thinking that your gambling will get under control as soon as you have a "big win".</li>
                <li>Constantly thinking or talking about gambling.</li>
              </ul>
            </p>
            <p><strong>Remember your loved ones will suffer too if your playing affects your family lifestyle and finances. Do not let yourself and your loved ones become casualties of gambling.</strong></p>
          </article>
          <aside class="col-sm-4 sidebar sidebar-right">
            <div class="widget">
   <div class="panel panel-info" style="border-radius:0"><div class="panel-heading">Draw No : 354<br>Saturday, 2020-06-13<img src="{{ asset ('assets/wp-content/themes/lotteryv1/images/favicon.png') }}" class="img-head"/></div>

                    <div class="panel-body" style="padding:0">
                      <table class="table table-bordered text-center" style="margin:0">
                        <thead><tr class="bg-primary"><th>1st Prize</th><th>2nd Prize</th><th>3rd Prize</th></tr></thead>
                        <tbody><tr class="prize"><td>970921</td><td>525431</td><td>172884</td></tr></tbody>
                        <thead><tr class="bg-success"><th colspan="3">Starter Prizes</th></tr></thead>
                        <tbody>      <tr><td>474932</td><td>434252</td><td>783275</td></tr>      <tr><td>299406</td><td>207186</td><td>334266</td></tr> </tbody><thead><tr class="bg-danger"><th colspan="3">Consolation Prizes</th></tr></thead>
                        <tbody>      <tr><td>440156</td><td>668840</td><td>189368</td></tr>      <tr><td>122897</td><td>756000</td><td>876732</td></tr> </tbody> </table>
                    </div>
                  </div>
              
          </div>
          </aside>
        </div>
      </div>
 </div>
  <div class="clear"></div>
@endsection