@extends('layout.master')

@section('content')
    <div class="container">
			<h2 class="text-center white"><?= $days; ?>, <?= $tanggal; ?></h2>
			<div class="banner">LIVE DRAW Lotto Result : 
			<!--//<div class="start">17:00:00</div> - <div class="until">17:30:00</div> //--></div>
			<div class="col-xs-4 col-xs-push-2" style="padding:0">
            <div class="panel panel-warning" style="border-radius:0"">
        <div class="panel-heading latest">Draw No : 354<img src="{{ asset('assets/wp-content/themes/lotteryv1/images/logo-chicago.png') }}" class="img-head"/></div>

                    <div class="panel-body" style="padding:0">
                      <table class="table table-bordered text-center" style="margin:0">
                        <thead><tr class="bg-primary"><th>1st Prize</th><th>2nd Prize</th><th>3rd Prize</th></tr></thead>
                        <tbody><tr class="prize"><td>970921</td><td>525431</td><td>172884</td></tr></tbody>
                        </table>
                    </div>
                  </div>
			</div>
			<div class="col-xs-4 col-xs-push-2" style="padding:0">
				<div class="col-xs-12 intro text-center" style="margin:0;">
					<h3 class="draw"><u>Next Draw in</u></h3>
<div id="buriq"></div>
					<div class="clock">
					</div>
					<h3>Everyone have luck ...</h3>
					<p>Wanna check your lucky numbers ?</p>
										
					<form class="form-group-sm form-inline" style="margin-bottom: 20px;" method="POST">
						<input type="text" class="form-control luck" value="" readonly /><button type="submit" name="generate" class="btn btn-sm btn-success">Generate</button>
					</form>

					<h4>If you don't buy a ticket, how is lady luck going to find you ?</h4>
					<h3>Today could be the day</h3>
				</div>
			</div>
			<div class="clearfix"></div>										
			<div class="col-xs-12">
				<div class="col-xs-4 display">
					<img src="{{ asset('assets/wp-content/themes/lotteryv1/images/numberballs.png') }}" /><div class="text-image"><h4>What <b>Numbers</b> to buy</h4>Translate your imagination into your lucky numbers.</div>
				</div>
				<div class="col-xs-4 display2">
					<img src="{{ asset('assets/wp-content/themes/lotteryv1/images/win.png') }}" /><div class="text-image"><h4>Did I <b>Win</b> ?</h4>Match the number you bought with the winning prize.</div>
				</div>
				<div class="col-xs-4 display3">
					<img src="{{ asset('assets/wp-content/themes/lotteryv1/images/gold-chest.png') }}" /><div class="text-image"><h4>Big <b>Winnings</b></h4>Win big prizes. Buy your Lucky Numbers today.</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		
	            <div class="container content">
              <table id="example" class="uk-table uk-table-hover uk-table-striped" style="width:100%;color:white;">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>09.30</th>
                <th>11:30</th>
                <th>18:30</th>
                <th>20:00</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
            </tr>
            
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
            </tr>
        </tfoot>
    </table>
                </div>
	<div class="clearfix"></div>
  
@endsection