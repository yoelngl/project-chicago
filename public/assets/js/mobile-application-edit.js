/**
 * Script to give preview images to all the added banned under: /mobile-application/
 *
 * It also places the Default and Active header above the appropriate fields to
 * split the data up.
 *
 */

 if (typeof jQuery == 'function') {

    if ($('body.section-mobile-application').length) {
        var fields = [
            $('#form-widgets-defaultBanner1SrcAPP'),
            $('#form-widgets-activeBanner1SrcAPP'),
            $('#form-widgets-defaultBanner2SrcAPP'),
            $('#form-widgets-activeBanner2SrcAPP'),
            $('#form-widgets-defaultBanner3SrcAPP'),
            $('#form-widgets-activeBanner3SrcAPP')
        ];

        $(fields).each(function(k, field) {
            var $field = $(field);

            // Add events so when the image url is updated, the preview is also updated.
            $field.on('blur', function() {
                update_image($(this));
            });
            update_image($(this));

            // Insert the headers above each section
            if ($field.attr('id').indexOf('default') > -1) {
                $('<h1 style="margin-bottom: 0;">Default</h1>').insertBefore($field.parent());
            }
            else if ($field.attr('id').indexOf('active') > -1) {
                $('<h1 style="margin-bottom: 0;">Active</h1>').insertBefore($field.parent());
            }
        });
    }
}


function update_image($el) {
    var $preview = $el.siblings('.image-preview');
    if ($preview.length == 0) {
        $preview = $('<div class="image-preview" style="float: right; max-height: 150px;"></div>');
        $preview.insertAfter($el);
    }

    if ($el.val().length == 0) {
        $preview.hide();
        return;
    }

    $preview.empty();
    $preview.append($('<img style="max-height: 150px" />').attr('src', $el.val()));
    $preview.show();
}
