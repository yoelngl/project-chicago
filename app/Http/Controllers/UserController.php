<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Number;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB; 

class UserController extends Controller
{
       protected function index()
    {
    	
    	
        $tadi      = Carbon::now('-5:00')->format('M j');
        $ytd      = Carbon::yesterday('-5:00')->format('M j');
        $ytd2     = Carbon::yesterday('-5:00')->add(-1, 'day')->format('M j');
        $ytd3     = Carbon::yesterday('-5:00')->add(-2, 'day')->format('M j');
        $ytd4     = Carbon::yesterday('-5:00')->add(-3, 'day')->format('M j');
        $ytd5     = Carbon::yesterday('-5:00')->add(-4, 'day')->format('M j');
        $ytd6     = Carbon::yesterday('-5:00')->add(-5, 'day')->format('M j');
        $ytd7     = Carbon::yesterday('-5:00')->add(-6, 'day')->format('M j');

        $tanggal = Carbon::now('-5:00')->format('F d, Y');

        $days =   Carbon::now('-5:00')->englishDayOfWeek;
        $now = Carbon::now('-5:00')->format('H:i:s a');
        $morning = Carbon::createFromTimeString(9, 30, 0, '-5:00')->format('H:i:s a');
        $morning_until = Carbon::createFromTimeString(11, 29, 59,'-5:00')->format('H:i:s a');
        $midday = Carbon::createFromTimeString(11, 30, 0,'-5:00')->format('H:i:s a');
        $midday_until = Carbon::createFromTimeString(18, 29, 59,'-5:00')->format('H:i:s a');
        $evening = Carbon::createFromTimeString(18, 30, 0,'-5:00')->format('H:i:s a');
        $evening_until = Carbon::createFromTimeString(19, 0, 0,'-5:00')->format('H:i:s a');
        $night = Carbon::createFromTimeString(20, 0, 0,'-5:00')->format('H:i:s a');
        $night_until = Carbon::createFromTimeString(22, 0, 0,'-5:00')->format('H:i:s a');

        $seven = Carbon::createFromTime(19, 0, 0, '-5:00')->format('H:i:s a');
        $seven_until = Carbon::createFromTime(19, 59, 59, '-5:00')->format('H:i:s a');

        $eleven = Carbon::createFromTime(22, 0, 0, '-5:00')->format('H:i:s a');
        $eleven_until = Carbon::createFromTime(23, 59, 59, '-5:00')->format('H:i:s a');
        $noon = Carbon::createFromTimeString(00, 0, 0,'-5:00')->format('H:i:s a');
        $noon_until = Carbon::createFromTimeString(9, 0, 0,'-5:00')->format('H:i:s a');
        $nine = Carbon::createFromTimeString(9, 0, 0,'-5:00')->format('H:i:s a');
        $nine_until = Carbon::createFromTimeString(9, 30, 0,'-5:00')->format('H:i:s a');
        if($now >= $morning && $now <= $morning_until){
            $hari = '09:30 (CDT)';
        }
        else if($now >= $midday && $now <= $midday_until){
            $hari = '11:30 (CDT)';
        }
        else if($now >= $evening && $now <= $evening_until){
            $hari = '18:30 (CDT)';
        }
        else if($now >= $seven && $now <= $seven_until){
            $hari = '18:30 (CDT)';
        }
        else if($now >= $night && $now <= $night_until){
            $hari = '20:00 (CDT)';
        }
        else if($now >= $eleven && $now <= $eleven_until){
            $hari = '20:00 (CDT)';
        }
        else if($now >= $noon && $now <=  $noon_until){
            $hari = '20:00 (CDT)';
        }

        $data = $this->allData();
        $pagi = $this->morning();
        $siang = $this->midday();
        $sore = $this->evening();
        $malam = $this->night();
        $all = $this->tgl();
        $pagiTadi = $this->tadiPagi();
        $siangTadi = $this->tadiSiang();
        $soreTadi = $this->tadiSore();
        $malamTadi = $this->tadiMalam();

        return view('template.home', compact('data','hari' ,'days','tanggal', 'pagi', 'siang', 'sore', 'malam', 'all', 'ytd', 'ytd2', 'ytd3', 'ytd4', 'ytd5', 'ytd6', 'ytd7', 'tadi', 'pagiTadi', 'siangTadi', 'soreTadi', 'malamTadi'));
    }

    public function allData()
    {
        $data = Number::all();
        $today = Carbon::now('-5:00')->format('H:i:s a');

       

        $day1 = Carbon::createFromTime(9, 30, 00,'-5:00')->format('H:i:s a');
        $day2 = Carbon::createFromTime(11, 29, 59,'-5:00')->format('H:i:s a');

        $day3 = Carbon::createFromTime(11, 30, 00,'-5:00')->format('H:i:s a');
        $day4 = Carbon::createFromTime(18, 29, 59,'-5:00')->format('H:i:s a');

        $day5 = Carbon::createFromTime(18, 30, 00,'-5:00')->format('H:i:s a');
        $day6 = Carbon::createFromTime(19, 59, 59,'-5:00')->format('H:i:s a');

        $day7 = Carbon::createFromTime(20, 00, 00, '-5:00')->format('H:i:s a');
        $day8 = Carbon::createFromTime(23, 59, 59, '-5:00')->format('H:i:s a');

        $day9 = Carbon::createFromTime(00, 00, 00, '-5:00')->format('H:i:s a');
        $day10 = Carbon::createFromTime(9, 29, 59,'-5:00')->format('H:i:s a');
        
        


        // $times = '';
        if($today >= $day1 && $today <= $day2)
        {
            $data = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '09:30 AM')->get();
            // $times = 'time 1';
        }
        elseif($today >= $day3 && $today <= $day4)
        {
            $data = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '11:30 AM')->get();
            // $times = 'time 2';
        }
        elseif($today >= $day5 && $today <= $day6)
        {
            $data = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '06:30 PM')->get();
            // $times = 'time 3';
        }
        elseif($today >= $day7 && $today <= $day8)
        {
            $data = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '08:00 PM')->get();
            // $times = 'time 4';
        }
        elseif($today >= $day9 && $today <= $day10)
        {
            $data = Number::whereDate('tanggal', Carbon::yesterday('-5:00')->format('Y-m-d'))->where('time', '08:00 PM')->get();
            // $times = 'time 4';
        }

        return $data;
    }

    public function morning()
    {
        $pagi = Number::where('time', '09:30 AM')->get();
        return $pagi;
    }

    public function midday()
    {
        $siang = Number::where('time', '11:30 AM')->get();
        return $siang;
    }

    public function evening()
    {
        $sore = Number::where('time', '06:30 PM')->get();
        return $sore;
    }

    public function night()
    {
        $malam = Number::where('time', '08:00 PM')->get();
        return $malam;
    }

    public function tgl()
    {
        $all = Number::groupBy('tanggal')->get();
        return $all;
    }

    public function result()
    {
        $results = Number::orderBy('tanggal', 'desc')->get();
        $skr = Carbon::now('-5:00')->format('Y-m-d');
        $now = Carbon::now('-5:00')->format('l, F d');
        $ytd = Carbon::yesterday('-5:00')->format('Y-m-d');
        $tom = Carbon::now('-5:00')->add(1, 'day')->format('Y-m-d');
        return view('template.result.result', compact('skr', 'now', 'ytd', 'tom', 'results'));
    }

    public function tadiPagi() {
      if (Carbon::now('-5:00')->format('H:i:s a') >= Carbon::createFromTime(9, 30, 00, '-5:00')->format('H:i:s a')) {
         $pagiTadi = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '09:30 AM')->get();

         foreach ($pagiTadi as $key => $value) {
            return $value->angka_1;
         }
      }
   }

   public function tadiSiang() {
      if (Carbon::now('-5:00')->format('H:i:s a') >= Carbon::createFromTime(11, 30, 00, '-5:00')->format('H:i:s a')) {
         $siangTadi = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '11:30 AM')->get();

         foreach ($siangTadi as $key => $value) {
            return $value->angka_1;
         }
      }
   }

   public function tadiSore() {
      if (Carbon::now('-5:00')->format('H:i:s a') >= Carbon::createFromTime(18, 30, 00, '-5:00')->format('H:i:s a')) {
         $soreTadi = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '06:30 PM')->get();

         foreach ($soreTadi as $key => $value) {
            return $value->angka_1;
         }
      }
   }

   public function tadiMalam() {
      if (Carbon::now('-5:00')->format('H:i:s a') >= Carbon::createFromTime(20, 00, 00, '-5:00')->format('H:i:s a')) {
         $malamTadi = Number::whereDate('tanggal', Carbon::now('-5:00')->format('Y-m-d'))->where('time', '08:00 PM')->get();

         foreach ($malamTadi as $key => $value) {
            return $value->angka_1;
         }
      }
   }
}
