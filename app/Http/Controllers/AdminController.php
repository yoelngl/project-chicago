<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Number;
use Validator;
use Carbon\Carbon;
use App\Model\Admin;
use Auth;


class AdminController extends Controller
{
    public function dashboard(){
        $data = Number::orderBy('tanggal', 'desc')->get();
        return view('admin',compact('data'));
    }

    public function add(Request $request) {
        $rules = array(
            'tanggal'   => 'required',
            'waktu'     => 'required',
            'no_satu'   => 'required',
            'no_dua'    => 'required',
            'no_tiga'   => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'tanggal'   => $request->tanggal,
            'time'     => $request->waktu,
            'angka_1'   => $request->no_satu,
            'angka_2'    => $request->no_dua,
            'angka_3'   => $request->no_tiga,
        );
        // dd($form_data);
        if (Number::whereDate('tanggal', $request->tanggal)->where('time', $request->waktu)->exists()) {
            return response()->json(['error' => 'Data Exist!']);
        } else {
            Number::create($form_data);
        }

        return response()->json(['success' => 'Data Added']);
    }

    public function update(Request $request) {
        $rules = array(
            'tanggal'   => 'required',
            'waktu'     => 'required',
            'no_satu'   => 'required',
            'no_dua'    => 'required',
            'no_tiga'   => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'tanggal'   => $request->tanggal,
            'time'     => $request->waktu,
            'angka_1'   => $request->no_satu,
            'angka_2'    => $request->no_dua,
            'angka_3'   => $request->no_tiga,
        );
        // dd($form_data);
        if (Number::whereDate('tanggal', $request->tanggal)->where('time', $request->waktu)->exists() == $request->hidden_id)  {
            Number::whereId($request->hidden_id)->update($form_data);
            return response()->json(['success' => 'Data Updated']);
        }else if(Number::whereDate('tanggal', $request->tanggal)->where('time', $request->waktu)->exists() != $request->hidden_id){
            Number::whereId($request->hidden_id)->update($form_data);
            return response()->json(['success' => 'Data Updated']);
                }
        else {
            return response()->json(['error' => 'Data Exist!']);
        }
    }

    public function edit($id) {
        $data = Number::findOrFail($id);
        return response()->json(['result' => $data]);
    }

    public function delete($id)
    {
        $data = Number::where('id', $id)->delete();
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function change(Request $request) {
        $idUser = Admin::where('id', Auth::user()->id)->update([
            'password' => bcrypt($request->password),
        ]);

        return response()->json(['success' => $idUser]);
    }


}
