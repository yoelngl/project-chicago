<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $table = 'number';

    protected $guarded = [''];

    protected $dates = ['tanggal'];
    
}
