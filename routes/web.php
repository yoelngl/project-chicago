<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::GET('/','UserController@index')->name('home');
Route::get('/contact', function () {
    return view('templates.contact.contact');
});
Route::get('/responsible', function () {
    return view('templates.responsible.responsible');
});
Route::GET('/result','UserController@result')->name('result');
Route::get('/winning-numbers', function () {
    return view('templates.winning-numbers.winning-numbers');
});

Route::GET('backend','Auth\LoginController@show')->name('login');
Route::POST('login','Auth\LoginController@login')->name('login');
Route::GET('logout','Auth\LoginController@logout')->name('logout');
// Auth::routes();
Route::group(['middleware' => ['auth' => 'auth:admin']], function () {
    Route::GET('/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::POST('admin/add','AdminController@add')->name('admin.add');
    Route::POST('admin/update','AdminController@update')->name('admin.update');
    Route::GET('admin/edit/{id}','AdminController@edit')->name('admin.edit');
    Route::POST('admin/delete/{id}','AdminController@delete')->name('admin.delete');
    Route::POST('admin/change','AdminController@change')->name('admin.change');



});
// Route::get('/home', 'HomeController@index')->name('home');
